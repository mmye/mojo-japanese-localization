use strict;
use warnings;
use File::Find 'find';
use File::Copy 'copy';

find (
  sub {
    my $src = $File::Find::name;
    my $dist = $src;

    if( -d $dist ) {

    } elsif ( $dist =~ /\.pod$/ || $dist =~ /\.pm$/ ) {
      $dist =~ s/\//-/g;
      $dist =~ s/\.pod$/.txt/;
      $dist =~ s/\.pm$/.txt/;
      $dist =~ s/^\.-//;
      print "dist: $dist\n";
      print "source: $src\n";
      copy $src, $dist or die $!;
    }
  }, '.'
)  
