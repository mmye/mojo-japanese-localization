use strict;
use warnings;
use File::Copy 'copy';

my @srcs = glob '*.pod';

for my $src (@srcs) {
  my $dist = $src;
  if ( -d $dist ) {
    return;
  }
  $dist =~ s/\.pod$/.txt/;
  copy $src, $dist or die;
}
